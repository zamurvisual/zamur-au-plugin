# `zamur-au-plugin`

## Consume Plugin

You can consume this plugin directly by:

### from bitbucket repository

npm i bitbucket:zamurvisual/zamur-au-plugin

### from published npm package

https://www.npmjs.com/package/zamur-au-plugin

Then load the plugin in app's `main.js` like this.

```js
aurelia.use.plugin("zamur-au-plugin");
// for webpack user, use PLATFORM.moduleName wrapper
aurelia.use.plugin(PLATFORM.moduleName("zamur-au-plugin"));
```

## Helpers

https://aurelia.io/docs/plugins/write-new-plugin#develop-plugin
