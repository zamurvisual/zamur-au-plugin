export class UpcaseValueConverter {
  toView(value) {
    return typeof value === 'string' ? value.toUpperCase() : '';
  }
}
